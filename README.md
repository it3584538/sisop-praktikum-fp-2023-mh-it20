# sisop-praktikum-fp-2023-MH-IT20
Laporan pengerjaan soal Final Project Praktikum Sistem Operasi 2023 Kelompok IT20

## Anggota Kelompok:
| N0. | Nama Anggota | NRP |
|-----| ----------- | ----------- |
| 1. | M. Januar Eko Wicaksono | 5027221006 |
| 2. | Rizki Ramadhani | 5027221013 |
| 3. |Khansa Adia Rahma | 5027221071 | 

## Soal Final Project

### Sistem Database Sederhana
---

### Bagaimana Program Diakses.
---
- Program server berjalan sebagai daemon.
- Untuk bisa akses console database, perlu buka program client (kalau di Linux seperti command mysql di bash).
- Program client dan utama berinteraksi lewat socket.
- Program client bisa mengakses server dari mana saja.


### Struktur Direktori
---
```c
# Struktur repository (di repository Gitlab)
-database/
--[program_database].c
--[settingan_cron_backup]
-client/
--[program_client].c
-dump/
--[program_dump_client].c
```

- Penamaan dari program bebas (database.c)
```c
# Struktur server (ketika program dijalankan)
[folder_server_database]
-[program_database]
-database/
--[nama_database]/  → Directory
---[nama_tabel]         → File
```

```c
# Contoh struktur untuk server
database/
-program_databaseku
-databases/
--database1/
---table11
---table12
--database2/
---table21
---table22
---table23
```
- Bagaimana struktur data saat tersimpan di dalam file tabel bebas.

### Bagaimana Database Digunakan
---

A. Autentikasi
- Terdapat user dengan username dan password  yang digunakan untuk mengakses database yang merupakan haknya (database yang memiliki akses dengan username tersebut). Namun, jika user merupakan root (sudo), maka bisa mengakses semua database yang ada.
Catatan: Untuk hak tiap user tidak perlu didefinisikan secara rinci, cukup apakah bisa akses atau tidak.
```c
# Format

./[program_client_database] -u [username] -p [password]
```
```c
# Contoh

./client_databaseku -u john -p john123
```
- Username, password, dan hak akses database disimpan di suatu database juga, tapi tidak ada user yang bisa akses database tersebut kecuali mengakses menggunakan root.
- User root sudah ada dari awal.
```c
# Contoh cara user root mengakses program client

sudo ./client_database
```

- Menambahkan user (Hanya bisa dilakukan user root)
```c
# Format

CREATE USER [nama_user] IDENTIFIED BY [password_user];
```
```c
# Contoh

CREATE USER khonsu IDENTIFIED BY khonsu123;
```

B. Autorisasi
- Untuk dapat mengakses database yang dia punya, permission dilakukan dengan command. Pembuatan tabel dan semua DML butuh untuk mengakses database terlebih dahulu.
```c
# Format

USE [nama_database];
```
```c
# Contoh

USE database1;
```
- Yang bisa memberikan permission atas database untuk suatu user hanya root.
```c
# Format

GRANT PERMISSION [nama_database] INTO [nama_user];
```
```c
# Contoh

GRANT PERMISSION database1 INTO user1;
```
- User hanya bisa mengakses database di mana dia diberi permission untuk database tersebut.

C. Data Definition Language
- Input penamaan database, tabel, dan kolom hanya angka dan huruf.
- Semua user bisa membuat database, otomatis user tersebut memiliki permission untuk database tersebut.
```c
# Format

CREATE DATABASE [nama_database];
```
```c
# Contoh

CREATE DATABASE database1;
```
- Root dan user yang memiliki permission untuk suatu database untuk bisa membuat tabel untuk database tersebut, tentunya setelah mengakses database tersebut. Tipe data dari semua kolom adalah string atau integer. Jumlah kolom bebas.
```c
# Format

CREATE TABLE [nama_tabel] ([nama_kolom] [tipe_data], ...);\
```
```c
# Contoh

CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);
```
- Bisa melakukan DROP database, table (setelah mengakses database), dan kolom. Jika sasaran drop ada maka di-drop, jika tidak ada maka biarkan.
```c
# Format

DROP [DATABASE | TABLE | COLUMN] [nama_database | nama_tabel | [nama_kolom] FROM [nama_tabel]];
```
```c
# Contoh

## Drop database

DROP DATABASE database1;

## Drop table

DROP TABLE table1;

## Drop Column

DROP COLUMN kolom1 FROM table1;
```

D. Data Manipulation Language.

- INSERT
Hanya bisa insert satu row per satu command. Insert sesuai dengan jumlah dan urutan kolom.
```c
# Format

INSERT INTO [nama_tabel] ([value], ...);
```
```c
# Contoh

INSERT INTO table1 (‘value1’, 2, ‘value3’, 4);
```

- UPDATE
Hanya bisa update satu kolom per satu command.
```c
# Format

UPDATE [nama_tabel] SET [nama_kolom]=[value];
```
```c
# Contoh

UPDATE table1 SET kolom1=’new_value1’;
```

- DELETE
Delete data yang ada di tabel.
```c
# Format

DELETE FROM [nama_tabel];
```
```c
# Contoh

DELETE FROM table1;
```

- SELECT
```c
# Format

DELETE FROM [nama_tabel];
```
```c
# Contoh 1

SELECT kolom1, kolom2 FROM table1;

# Contoh 2

SELECT * FROM table1;
```

- WHERE
Command UPDATE, SELECT, dan DELETE bisa dikombinasikan dengan WHERE. WHERE hanya untuk satu kondisi. Dan hanya ‘=’.
```c
# Format

[Command UPDATE, SELECT, DELETE] WHERE [nama_kolom]=[value];
```
```c
# Contoh

DELETE FROM table1 WHERE kolom1=’value1’;
```

E. Logging.
- Setiap command yang dipakai harus dilakukan logging ke suatu file dengan format. Jika yang eksekusi root, maka username root.
```c
# Format di dalam log

timestamp(yyyy-mm-dd hh:mm:ss):username:command
```
```c
# Contoh

2021-05-19 02:05:15:khonsu:SELECT FROM table1
```

F. Realibility
Harus membuat suatu program terpisah untuk dump database ke command-command yang akan di print ke layar. Untuk memasukkan ke file, gunakan redirection. Program ini tentunya harus melalui proses autentikasi terlebih dahulu. Ini sampai database level saja, tidak perlu sampai tabel.
```c
# Format

./[program_dump_database] -u [username] -p [password] [nama_database]
```
```c
# Contoh

./databasedump -u khonsu -p khonsu123 database1 > database1.backup
```
Contoh hasil isi file database1.backup adalah sebagai berikut.
```c
DROP TABLE table1;
CREATE TABLE table1 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table1 (‘abc’, 1, ‘bcd’, 2);

DROP TABLE table2;
CREATE TABLE table2 (kolom1 string, kolom2 int, kolom3 string, kolom4 int);

INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
INSERT INTO table2 (‘abc’, 1, ‘bcd’, 2);
```
- Program dump database dijalankan tiap jam untuk semua database dan log, lalu di zip sesuai timestamp, lalu log dikosongkan kembali.

G. Tambahan
- Kita bisa memasukkan command lewat file dengan redirection di program client.
```c
# Contoh

./client_databaseku -u john -p john123 -d database1 < database.backup
```

H. Error Handling.
Jika ada command yang tidak sesuai penggunaannya. Maka akan mengeluarkan pesan error tanpa keluar dari program client.

### Solution
---


# A. Autentikasi

## A1. Client authentication

Untuk melakukan autentikasi, maka dari client akan menerima 5 argumen dengan urutan `nama_program -u nama_user -p password_user` yang kemudian akan dikirim ke server dengan perintah `AUTH USER nama_user IDENTIFIED BY password_user` menggunakan kode berikut.

```c
strcpy(user_name, argv[2]);
strcpy(user_password, argv[4]);

char *request, response[BUFFER_SIZE];
request = format_string("%s;\nAUTH USER %s IDENTIFIED BY %s;", user_name, user_name, user_password);
send(sock, request, BUFFER_SIZE, 0);

recv(sock, response, BUFFER_SIZE, 0);
if (!strstr(response, "OK"))
{
    printf("%s\n", response);
    return EXIT_FAILURE;
}
free(request);
```

## A2. Create user

Pertama request dari client akan dipisah sesuai yang dibutuhkan menggunakan kode berikut.

```c
if (strcmp(perintah[0], "CREATE") == 0)
		{
			if (strcmp(perintah[1], "USER") == 0 && strcmp(perintah[3], "IDENTIFIED") == 0 && strcmp(perintah[4], "BY") == 0)
			{
				snprintf(buffer, sizeof buffer, "cUser:%s:%s:%d", perintah[2], perintah[5], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			}

			else if (strcmp(perintah[1], "TABLE") == 0)
			{
				snprintf(buffer, sizeof buffer, "cTable:%s", copyinput);
				send(clientSocket, buffer, strlen(buffer), 0);
			}

			else if (strcmp(perintah[1], "DATABASE") == 0)
			{
				snprintf(buffer, sizeof buffer, "cDatabase:%s:%s:%d", perintah[2], argv[2], id_user);
				send(clientSocket, buffer, strlen(buffer), 0);
			}
		}
```

Kemudian perlu dicek apakah user sudah ada atau belum dengan kode berikut.

```c
char* check_user = format_string(
    "grep -wq '%s,%s' '%s'",
    user_name, user_password, users_path
);

if (system(check_user) == 0) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: User already exists"
    );
    return EXIT_FAILURE;
}
free(check_user);
```

Setelah itu, user baru akan ditambahkan ke dalam tabel database user menggunakan kode berikut.

```c
char* add_user = format_string(
    "echo '%s,%s' >> '%s'",
    user_name, user_password, users_path
);
system(add_user);
free(add_user);
```

Berikut adalah hasil autentikasi yang berhasil dan juga gagal.
![Screenshot from 2023-06-25 20-24-44](https://github.com/mvinorian/hellscape/assets/54766683/89100238-5805-4ee5-9c35-501de0c93a3b)

# B. Autorisasi

## B1. Grant Permission

```c
	// minta akses grant permission
    else if (strcmp(perintah[0], "GRANT") == 0 && strcmp(perintah[1], "PERMISSION") == 0 && strcmp(perintah[3], "INTO") == 0)
		{
			snprintf(buffer, sizeof buffer, "gPermission:%s:%s:%d", perintah[2], perintah[4], id_user);
			send(clientSocket, buffer, strlen(buffer), 0);
		}

    // check user permission
    char *check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_name, permissions_path);

    if (system(check_user_permission) == 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User already has access to database");
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // add user permission
    char *add_user_permission = format_string(
        "echo '%s,%s' >> '%s'",
        database_name, user_name, permissions_path);
    system(add_user_permission);
    free(add_user_permission);

    sprintf(response, "GRANT PERMISSION OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Menambahkan permission ke user agar user dapat menggunakan database yang diijinkan. Grant permisson dapat dilakukan dengan menjalankan command `GRANT PERMISSION database_name INTO user_name`.

![Screenshot from 2023-06-26 05-35-11](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/3a62eb84-eef2-4eaa-8a2c-b61dc1ca2dc8)

## B2. Use Database

```c
int _use_database(char *request, char *response)
{
    // parsing request
    char database_name[BUFFER_SIZE];

    char *parse_str = "USE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    if (parsed_items != 1)
    {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "USE \e[3mdatabase_name\e[0m;");
        return EXIT_FAILURE;
    }

    // check database
    char *database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) != 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database doesn't exist");
        return EXIT_FAILURE;
    }
    free(database_path);

    // check user permission
    char *check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_client, permissions_path);

    if (system(check_user_permission) != 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User has no access to use this database");
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // change database_used
    if (database_used != NULL)
        free(database_used);
    database_used = format_string("%s", database_name);

    sprintf(response, "USE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}

```

Use database digunakan agar user dapat melakukan data manipulation pada database tersebut, dan use database hanya bisa digunakan apabila user telah memiliki grant permission ke database tersebut. Use database dapat dilakukan dengan menjalan command `USE database_name`

![Screenshot from 2023-06-26 05-39-49](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/fb39e8dd-4e41-442a-8afc-2ad2d8f08354)

## C. Data Definition Language

## C1. Create Database

```c

int _create_database(char *request, char *response)
{
    // parsing request
    char database_name[BUFFER_SIZE];

    char *parse_str = "CREATE DATABASE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    if (parsed_items != 1)
    {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "CREATE DATABASE \e[3mdatabase_name\e[0m;");
        return EXIT_FAILURE;
    }

    // check database
    char *database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) == 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database already exists");
        return EXIT_FAILURE;
    }

    // create database
    mkdir(database_path, 0777);
    free(database_path);

    // add user database permission
    char *add_permission;
    if (strcmp(user_client, "root") == 0)
        add_permission = format_string(
            "echo '%s,%s' >> '%s'",
            database_name, user_client, permissions_path);
    else
        add_permission = format_string(
            "echo '%s,%s\n%s,root' >> '%s'",
            database_name, user_client, database_name, permissions_path);
    system(add_permission);
    free(add_permission);

    sprintf(response, "CREATE DATABASE OK\n");
    // message_log(request);
    return EXIT_SUCCESS;
}
```

![Screenshot from 2023-06-26 05-39-49](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/fb39e8dd-4e41-442a-8afc-2ad2d8f08354)

maka user yang melakukan create database juga akan otomatis memiliki grant permission ke database tersebut.

![Screenshot from 2023-06-26 05-44-43](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/f56b0881-3a94-40d4-a9c1-adb453d9c850)

Database berhasil dibuat:

![Screenshot from 2023-06-26 05-42-53](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/31c4cce3-df03-4784-a61c-cf8c188f4058)

## C2. Create Table

```c
int _create_table(char *request, char *response)
{
    // check database_used
    if (database_used == NULL)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used");
        return EXIT_FAILURE;
    }

    // parsing request
    char table_name[BUFFER_SIZE];
    char constraints[BUFFER_SIZE];
    char *parse_str = "CREATE TABLE %s (%[^)]);";
    int parsed_items = sscanf(request, parse_str, table_name, constraints);

    if (parsed_items != 2)
    {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "CREATE TABLE \e[3mtable_name\e[0m (\e[3mcolumn_name\e[0m \e[3mcolumn_type\e[0m, ...);");
        return EXIT_FAILURE;
    }

    // check table
    char *table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) == 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table already exists");
        return EXIT_FAILURE;
    }

    // create table
    FILE *table = fopen(table_path, "w");

    char *column_pair = strtok(constraints, ",");
    while (column_pair != NULL)
    {
        char column_name[BUFFER_SIZE];
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, "%s %s", column_name, column_type);
        if (
            strcmp(column_type, "int") != 0 &&
            strcmp(column_type, "string") != 0)
        {
            sprintf(
                response, "%s\n%s\n",
                "\e[91mError\e[39m: Invalid column type",
                "\e[3mcolumn_type\e[0m = { int, string }");
            fclose(table);
            remove(table_path);
            return EXIT_FAILURE;
        }

        column_pair = strtok(NULL, ",");
        fprintf(table, "%s %s", column_name, column_type);
        if (column_pair != NULL)
            fprintf(table, ",");
    }
    fprintf(table, "\n");

    fclose(table);
    free(table_path);
    sprintf(response, "CREATE TABLE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}

```

create table dapat dilakukan dengan command `CREATE TABLE table_name (column1 datatype, column2 datatype)`. Tipe data hanya ada dua yaitu `int` dan `string`.

hasil dari create table:

![Screenshot from 2023-06-26 05-47-48](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/fa566443-216d-40a1-853e-c326aa37a615)

![Screenshot from 2023-06-26 05-47-52](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/2c0ea434-5f6f-447f-9daa-88929380cdf0)

## C3. Drop Database

```c
int _drop_database(char *request, char *response)
{
    // parse request
    char database_name[BUFFER_SIZE];

    char *parse_str = "DROP DATABASE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, database_name);

    if (parsed_items != 1)
    {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "DROP DATABASE \e[3mdatabase_name\e[0m;");
        return EXIT_FAILURE;
    }

    // check permissions
    char *check_user_permission = format_string(
        "grep -wq '%s,%s' '%s'",
        database_name, user_client, permissions_path);

    if (system(check_user_permission) != 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: User has no access to drop this database");
        return EXIT_FAILURE;
    }
    free(check_user_permission);

    // check database
    char *database_path = format_string("%s/%s", databases_path, database_name);

    if (access(database_path, F_OK) != 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Database doesn't exist");
        return EXIT_FAILURE;
    }

    // remove database
    char *remove_database = format_string("rm -rf '%s'", database_path);
    system(remove_database);
    free(remove_database);

    // revoke permission
    char *temp_permission_path = format_string("%s.tmp", permissions_path);

    char *revoke_permission = format_string(
        "grep -v '%s,' '%s' > '%s'",
        database_name, permissions_path, temp_permission_path);
    system(revoke_permission);
    free(revoke_permission);

    remove(permissions_path);
    rename(temp_permission_path, permissions_path);
    free(temp_permission_path);

    // change database used
    if (database_used != NULL)
        free(database_used);
    database_used = NULL;

    sprintf(response, "DROP DATABASE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Drop database dapat dilakukan dengan menggunakan command `DROP DATABASE database_name`.

![Screenshot from 2023-06-26 05-52-18](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/e7cc294e-8dd8-4b3b-82c2-26f5da5de73a)

maka database akan terhapus

![Screenshot from 2023-06-26 05-52-15](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/4af72e23-8ced-4d34-bbec-122d35079004)

## C4. Drop Table

```c
int _drop_table(char *request, char *response)
{
    // check database_used
    if (database_used == NULL)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used");
        return EXIT_FAILURE;
    }

    // parse request
    char table_name[BUFFER_SIZE];

    char *parse_str = "DROP TABLE %[^ ;];";
    int parsed_items = sscanf(request, parse_str, table_name);

    if (parsed_items != 1)
    {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "DROP TABLE \e[3mtable_name\e[0m;");
        return EXIT_FAILURE;
    }

    // check table
    char *table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist");
        return EXIT_FAILURE;
    }

    // remove table
    char *remove_table = format_string("rm -rf '%s'", table_path);
    system(remove_table);
    free(remove_table);
    free(table_path);

    sprintf(response, "DROP TABLE OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}

```

Drop table dapat dilakukan dengan menggunakan command `DROP TABLE table_name`.

![Screenshot from 2023-06-26 06-02-36](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/76dcdb14-04ad-4aec-94b3-582c3c9eb989)
maka table akan terhapus

![Screenshot from 2023-06-26 06-02-38](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/27ba93c9-eec1-4732-9511-827903053108)

## C5. Drop Column

```c

int _drop_column(char *request, char *response)
{
    // check database_used
    if (database_used == NULL)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: No database used");
        return EXIT_FAILURE;
    }

    // parse request
    char column_name[BUFFER_SIZE];
    char table_name[BUFFER_SIZE];

    char *parse_str = "DROP COLUMN %s FROM %[^ ;];";
    int parsed_items = sscanf(request, parse_str, column_name, table_name);

    if (parsed_items != 2)
    {
        sprintf(
            response, "%s\n%s\n",
            "\e[91mError\e[39m: Invalid command",
            "DROP COLUMN \e[3mcolumn_name\e[0m FROM \e[3mtable_name\e[0m;");
        return EXIT_FAILURE;
    }

    // check table
    char *table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

    if (access(table_path, F_OK) != 0)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Table doesn't exist");
        return EXIT_FAILURE;
    }

    // open table
    char *temp_table_path = format_string("%s.tmp", table_path);

    FILE *table = fopen(table_path, "r");
    FILE *temp_table = fopen(temp_table_path, "w");

    // check column
    int column_index = 0;
    char row[BUFFER_SIZE];
    bool column_found = false;
    fscanf(table, " %[^\n]", row);

    char *column_pair = strtok(row, ",");
    while (column_pair != NULL)
    {
        char column[BUFFER_SIZE];
        sscanf(column_pair, "%s", column);

        if (strcmp(column, column_name) != 0)
        {
            if (column_index != 0)
                fprintf(temp_table, ",");
            fprintf(temp_table, "%s", column_pair);
        }
        else
            column_found = true;

        column_pair = strtok(NULL, ",");
        if (!column_found)
            column_index++;
    }
    fprintf(temp_table, "\n");

    if (!column_found)
    {
        sprintf(
            response, "%s\n",
            "\e[91mError\e[39m: Column doesn't exist");
        fclose(table);
        fclose(temp_table);
        return EXIT_FAILURE;
    }

    // remove column
    while (fscanf(table, " %[^\n]", row) != EOF)
    {
        int index = 0;
        char *cell = strtok(row, ",");
        while (cell != NULL)
        {
            if (index != column_index)
            {
                if (index != 0)
                    fprintf(temp_table, ",");
                fprintf(temp_table, "%s", cell);
            }

            cell = strtok(NULL, ",");
            index++;
        }
        fprintf(temp_table, "\n");
    }

    fclose(table);
    fclose(temp_table);

    // rename temp_table;
    remove(table_path);
    rename(temp_table_path, table_path);
    free(temp_table_path);
    free(table_path);

    sprintf(response, "DROP COLUMN OK\n");
    message_log(request);
    return EXIT_SUCCESS;
}
```

Drop kolom dapat dilakukan dengan menggunakan command `DROP COLUMN column_name FROM table_name`.

![Screenshot from 2023-06-26 05-58-15](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/2856ab0c-d70d-4db3-94a3-96eb704b4b56)

# D. Data Manipulation Language

## D1. Insert

Pertama perlu dilakukan pengecekan apakah terapat database yang digunakan atau tidak menggunakan kode berikut.

```c
if (database_used == NULL) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Kemudian request dari client akan dipisah sesuai yang dibutuhkan menggunakan kode berikut.

```c
char table_name[BUFFER_SIZE];
char row[BUFFER_SIZE];

char* parse_str = "INSERT INTO %s (%[^)]);";
int parsed_items = sscanf(request, parse_str, table_name, row);

if (parsed_items != 2) {
    sprintf(
        response, "%s\n%s\n",
        "\e[91mError\e[39m: Invalid command",
        "INSERT INTO \e[3mtable_name\e[0m (\e[3mvalue_column_1\e[0m, \e[3mvalue_column_2\e[0m, ...);"
    );
    return EXIT_FAILURE;
}
```

Setelah itu perlu dicek apakah tabel dari request ada atau tidak menggunakan kode berikut.

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Selanjutnya, nilai baris yang akan dimasukkan akan dipisah menggunakan kode berikut.

```c
int total_column = 0;
char new_row[BUFFER_SIZE][BUFFER_SIZE];
char* value = strtok(row, ",");
while (value != NULL) {
    remove_leading_spaces(value);
    remove_trailing_spaces(value);

    strcpy(new_row[total_column++], value);
    value = strtok(NULL, ",");
}
```

Kemudian akan dilakukan pengecekan tipe data dari tiap nilai baris yang akan dimasukkan menggunakan kode berikut.

```c
FILE* table = fopen(table_path, "a+");

int column_index = 0;
char columns[BUFFER_SIZE];
fscanf(table, " %[^\n]", columns);

char* column_pair = strtok(columns, ",");
while (column_pair != NULL) {
    char column_type[BUFFER_SIZE];
    sscanf(column_pair, " %*[^ ] %s", column_type);
    if (
        strcmp(column_type, "int") == 0 &&
        !is_int(new_row[column_index])
    ) {
        sprintf(
            response, "%s %s is not a valid int\n",
            "\e[91mError\e[39m:", new_row[column_index]
        );
        return EXIT_FAILURE;
    }
    else if (
        strcmp(column_type, "string") == 0 &&
        !is_string(new_row[column_index])
    ) {
        sprintf(
            response, "%s %s is not a valid string\n%s\n",
            "\e[91mError\e[39m:", new_row[column_index],
            "'\e[3mexample string\e[0m' is a valid string"
        );
        return EXIT_FAILURE;
    }

    column_pair = strtok(NULL, ",");
    column_index++;
}
```

Setelah semua pengecekan dilakukan, baris baru akan ditambahkan pada tabel dengan kode berikut.

```c
for (int i = 0; i < total_column; i++) {
    if (i != 0) fprintf(table, ",");
    fprintf(table, "%s", new_row[i]);
}
fprintf(table, "\n");
```

Berikut adalah hasil dari perintah insert.
![Screenshot from 2023-06-25 19-30-25](https://github.com/mvinorian/hellscape/assets/54766683/ae2116b6-a704-45ba-9949-3d421a1a5f69)

## D2. Update

Pertama perlu dilakukan pengecekan apakah terapat database yang digunakan atau tidak menggunakan kode berikut.

```c
if (database_used == NULL) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Kemudian request dari client akan dipisah sesuai yang dibutuhkan menggunakan kode berikut.

```c
char table_name[BUFFER_SIZE];
char column_name[BUFFER_SIZE];
char value[BUFFER_SIZE];
char where_column[BUFFER_SIZE];
char where_value[BUFFER_SIZE];
bool has_where = false;

char* parse_str = "UPDATE %s SET %[^=]= %[^;W]WHERE %[^=]= %[^;];";
int parsed_items = sscanf(
    request, parse_str,
    table_name, column_name, value, where_column, where_value
);

if (parsed_items != 3 && parsed_items != 5) {
    sprintf(
        response, "%s\n%s\n",
        "\e[91mError\e[39m: Invalid command",
        "UPDATE \e[3mtable_name\e[0m SET \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m [ WHERE \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m ];"
    );
    return EXIT_FAILURE;
}

has_where = parsed_items == 5;

remove_trailing_spaces(value);
if (has_where) remove_trailing_spaces(where_value);
```

Setelah itu perlu dicek apakah tabel dari request ada atau tidak menggunakan kode berikut.

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Kemudian kita cek apakah kolom yang akan diupdate valid atau tidak dengan kode berikut.

```c
char* temp_table_path = format_string("%s.tmp", table_path);

FILE* table = fopen(table_path, "r");
FILE* temp_table = fopen(temp_table_path, "w");

int index = 0;
int column_index = -1;
int where_column_index = -1;
char row[BUFFER_SIZE];
fscanf(table, " %[^\n]", row);

char* column_pair = strtok(row, ",");
while (column_pair != NULL) {
    char column[BUFFER_SIZE];
    char column_type[BUFFER_SIZE];
    sscanf(column_pair, "%s %s", column, column_type);

    if (strcmp(column, column_name) == 0) {
        column_index = index;
        if (
            strcmp(column_type, "int") == 0 &&
            !is_int(value)
        ) {
            sprintf(
                response, "%s %s is not a valid int\n",
                "\e[91mError\e[39m:", value
            );
            fclose(temp_table);
            remove(temp_table_path);
            return EXIT_FAILURE;
        }
        else if (
            strcmp(column_type, "string") == 0 &&
            !is_string(value)
        ) {
            sprintf(
                response, "%s %s is not a valid string\n%s\n",
                "\e[91mError\e[39m:", value,
                "'\e[3mexample string\e[0m' is a valid string"
            );
            fclose(temp_table);
            remove(temp_table_path);
            return EXIT_FAILURE;
        }
    }

    if (has_where && strcmp(column, where_column) == 0) {
        where_column_index = index;
        if (
            strcmp(column_type, "int") == 0 &&
            !is_int(where_value)
        ) {
            sprintf(
                response, "%s %s is not a valid int\n",
                "\e[91mError\e[39m:", where_value
            );
            fclose(temp_table);
            remove(temp_table_path);
            return EXIT_FAILURE;
        }
        else if (
            strcmp(column_type, "string") == 0 &&
            !is_string(where_value)
        ) {
            sprintf(
                response, "%s %s is not a valid string\n%s\n",
                "\e[91mError\e[39m:", where_value,
                "'\e[3mexample string\e[0m' is a valid string"
            );
            fclose(temp_table);
            remove(temp_table_path);
            return EXIT_FAILURE;
        }
    }

    if (index != 0) fprintf(temp_table, ",");
    fprintf(temp_table, "%s", column_pair);
    column_pair = strtok(NULL, ",");
    index++;
}
fprintf(temp_table, "\n");

if (column_index == -1) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Column doesn't exist"
    );
    fclose(table);
    fclose(temp_table);
    return EXIT_FAILURE;
}

if (has_where && where_column_index == -1) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Where column doesn't exist"
    );
    fclose(table);
    fclose(temp_table);
    return EXIT_FAILURE;
}
```

Setelah semua pengecekan dilakukan, baris akan diupdate dengan kode berikut.

```c
while (fscanf(table, " %[^\n]", row) != EOF) {
    bool is_valid_row = !has_where;
    if (has_where) {
        index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);
        char* cell = strtok(row_copy, ",");
        while (cell != NULL) {
            if (
                index == where_column_index &&
                strcmp(cell, where_value) == 0
            ) {
                is_valid_row = true;
                break;
            }
            cell = strtok(NULL, ",");
            index++;
        }
    }
    if (is_valid_row) {
        index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);
        char* cell = strtok(row_copy, ",");
        while (cell != NULL) {
            if (index != 0) fprintf(temp_table, ",");
            fprintf(
                temp_table, "%s",
                index == column_index
                ? value : cell
            );
            cell = strtok(NULL, ",");
            index++;
        }
    }
    else fprintf(temp_table, "%s", row);
    fprintf(temp_table, "\n");
}

fclose(table);
fclose(temp_table);

remove(table_path);
rename(temp_table_path, table_path);
free(temp_table_path);
free(table_path);
```

Berikut adalah hasil dari perintah update.
![Screenshot from 2023-06-25 19-37-32](https://github.com/mvinorian/hellscape/assets/54766683/78acd2d9-8d1d-4146-9884-987d822a04fa)

## D3. Delete

Pertama perlu dilakukan pengecekan apakah terapat database yang digunakan atau tidak menggunakan kode berikut.

```c
if (database_used == NULL) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Kemudian request dari client akan dipisah sesuai yang dibutuhkan menggunakan kode berikut.

```c
char table_name[BUFFER_SIZE];
char where_column[BUFFER_SIZE];
char where_value[BUFFER_SIZE];
bool has_where = false;

char* parse_str = "DELETE FROM %[^;W]WHERE %[^=]= %[^;];";
int parsed_items = sscanf(
    request, parse_str,
    table_name, where_column, where_value
);

if (parsed_items != 1 && parsed_items != 3) {
    sprintf(
        response, "%s\n%s\n",
        "\e[91mError\e[39m: Invalid command",
        "DELETE FROM \e[3mtable_name\e[0m [ WHERE \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m ];"
    );
    return EXIT_FAILURE;
}

has_where = parsed_items == 3;

remove_trailing_spaces(table_name);
if (has_where) remove_trailing_spaces(where_value);
```

Setelah itu perlu dicek apakah tabel dari request ada atau tidak menggunakan kode berikut.

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Setelah itu, kolom where yang akan dihapus akan dicek dengan kode berikut.

```c
int index = 0;
int where_column_index = -1;
char row[BUFFER_SIZE];
fscanf(table, " %[^\n]", row);

char* column_pair = strtok(row, ",");
while (column_pair != NULL) {
    char column_name[BUFFER_SIZE];
    char column_type[BUFFER_SIZE];
    sscanf(column_pair, "%s %s", column_name, column_type);

    if (has_where && strcmp(column_name, where_column) == 0) {
        where_column_index = index;
        if (
            strcmp(column_type, "int") == 0 &&
            !is_int(where_value)
        ) {
            sprintf(
                response, "%s %s is not a valid int\n",
                "\e[91mError\e[39m:", where_value
            );
            fclose(temp_table);
            remove(temp_table_path);
            return EXIT_FAILURE;
        }
        else if (
            strcmp(column_type, "string") == 0 &&
            !is_string(where_value)
        ) {
            sprintf(
                response, "%s %s is not a valid string\n%s\n",
                "\e[91mError\e[39m:", where_value,
                "'\e[3mexample string\e[0m' is a valid string"
            );
            fclose(temp_table);
            remove(temp_table_path);
            return EXIT_FAILURE;
        }
    }

    if (index != 0) fprintf(temp_table, ",");
    fprintf(temp_table, "%s", column_pair);
    column_pair = strtok(NULL, ",");
    index++;
}
fprintf(temp_table, "\n");

if (has_where && where_column_index == -1) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Where column doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Setelah semua pengecekan dilakukan, baris yang sesuai akan dihapus dengan kode berikut.

```c
while (fscanf(table, " %[^\n]", row) != EOF) {
    bool is_valid_row = !has_where;
    if (has_where) {
        int index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);
        char* cell = strtok(row_copy, ",");
        while (cell != NULL) {
            if (
                index == where_column_index &&
                strcmp(cell, where_value) == 0
            ) {
                is_valid_row = true;
                break;
            }
            cell = strtok(NULL, ",");
            index++;
        }
    }
    if (is_valid_row) continue;
    fprintf(temp_table, "%s\n", row);
}

fclose(table);
fclose(temp_table);

remove(table_path);
rename(temp_table_path, table_path);
free(temp_table_path);
free(table_path);
```

Berikut adalah hasil dari perintah delete.
![Screenshot from 2023-06-25 19-42-29](https://github.com/mvinorian/hellscape/assets/54766683/81473421-1bad-442e-a07e-4488e2f3a44a)

## D4. Select

Pertama perlu dilakukan pengecekan apakah terapat database yang digunakan atau tidak menggunakan kode berikut.

```c
if (database_used == NULL) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: No database used"
    );
    return EXIT_FAILURE;
}
```

Kemudian request dari client akan dipisah sesuai yang dibutuhkan menggunakan kode berikut.

```c
char select_columns[BUFFER_SIZE];
char table_name[BUFFER_SIZE];
char where_column[BUFFER_SIZE];
char where_value[BUFFER_SIZE];
bool select_all = false;
bool has_where = false;

char* parse_str = "SELECT %[^F]FROM %[^ ;]%*[; ] WHERE %[^=]= %[^;];";
int parsed_items = sscanf(
    request, parse_str,
    select_columns, table_name, where_column, where_value
);

if (parsed_items != 2 && parsed_items != 4) {
    sprintf(
        response, "%s\n%s\n",
        "\e[91mError\e[39m: Invalid command",
        "SELECT [ * | \e[3mcolumn_name\e[0m [, ...] ] FROM \e[3mtable_name\e[0m [ WHERE \e[3mcolumn_name\e[0m=\e[3mvalue\e[0m ];"
    );
    return EXIT_FAILURE;
}

has_where = parsed_items == 4;

if (has_where) remove_trailing_spaces(where_value);
remove_trailing_spaces(select_columns);

// set select all
select_all = strlen(select_columns) == 1 && select_columns[0] == '*';
```

Setelah itu perlu dicek apakah tabel dari request ada atau tidak menggunakan kode berikut.

```c
char* table_path = format_string("%s/%s/%s", databases_path, database_used, table_name);

if (access(table_path, F_OK) != 0) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Table doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Setelah itu, kolom yang dipilih akan dicek kevalidannya dengan kode berikut.

```c
FILE* table = fopen(table_path, "r");

int arg_column = 0;
int total_column = 0;
int column_id[BUFFER_SIZE];
char columns[BUFFER_SIZE][BUFFER_SIZE];

char row[BUFFER_SIZE];
fscanf(table, " %[^\n]", row);

strcpy(response, "");
if (select_all) {
    strcat(response, row);
}
else {
    char* column_name = strtok(select_columns, ",");
    while (column_name != NULL) {
        remove_leading_spaces(column_name);
        remove_trailing_spaces(column_name);

        strcpy(columns[arg_column++], column_name);
        column_name = strtok(NULL, ",");
    }
    for (int i = 0; i < arg_column; i++) {
        int index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);

        char* column_pair = strtok(row_copy, ",");
        while (column_pair != NULL) {
            char column[BUFFER_SIZE];
            sscanf(column_pair, "%s", column);
            if (strcmp(column, columns[i]) == 0)
            {
                column_id[total_column++] = index;
                if (i != 0) strcat(response, ",");
                strcat(response, column_pair);
                break;
            }
            column_pair = strtok(NULL, ",");
            index++;
        }
    }
}
strcat(response, "\n");

int column_index = 0;
int where_column_index = -1;
char* column_pair = strtok(row, ",");
if (has_where) {
    while (column_pair != NULL) {
        char column_name[BUFFER_SIZE];
        char column_type[BUFFER_SIZE];
        sscanf(column_pair, "%s %s", column_name, column_type);

        if (strcmp(column_name, where_column) == 0) {
            where_column_index = column_index;
            if (
                strcmp(column_type, "int") == 0 &&
                !is_int(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid int\n",
                    "\e[91mError\e[39m:", where_value
                );
                return EXIT_FAILURE;
            }
            else if (
                strcmp(column_type, "string") == 0 &&
                !is_string(where_value)
            ) {
                sprintf(
                    response, "%s %s is not a valid string\n%s\n",
                    "\e[91mError\e[39m:", where_value,
                    "'\e[3mexample string\e[0m' is a valid string"
                );
                return EXIT_FAILURE;
            }
        }

        column_pair = strtok(NULL, ",");
        column_index++;
    }
}

if (!select_all && total_column != arg_column) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Select has invalid column/s"
    );
    return EXIT_FAILURE;
}

if (has_where && where_column_index == -1) {
    sprintf(
        response, "%s\n",
        "\e[91mError\e[39m: Where column doesn't exist"
    );
    return EXIT_FAILURE;
}
```

Setelah semua pengecekan dilakukan, baris yang sesuai akan dikembalikan ke user sebagai response dengan kode berikut.

```c
while (fscanf(table, " %[^\n]", row) != EOF) {
    bool is_valid_row = !has_where;
    if (has_where) {
        int index = 0;
        char row_copy[BUFFER_SIZE];
        strcpy(row_copy, row);
        char* cell = strtok(row_copy, ",");
        while (cell != NULL) {
            if (
                index == where_column_index &&
                strcmp(cell, where_value) == 0
            ) {
                is_valid_row = true;
                break;
            }
            cell = strtok(NULL, ",");
            index++;
        }
    }
    if (is_valid_row) {
        if (select_all) strcat(response, row);
        else {
            for (int i = 0; i < total_column; i++) {
                int index = 0;
                char row_copy[BUFFER_SIZE];
                strcpy(row_copy, row);
                char* cell = strtok(row_copy, ",");
                while (cell != NULL) {
                    if (index == column_id[i]) {
                        if (i != 0) strcat(response, ",");
                        strcat(response, cell);
                        break;
                    }
                    cell = strtok(NULL, ",");
                    index++;
                }
            }
        }
        strcat(response, "\n");
    }
}

fclose(table);
free(table_path);
```

Berikut adalah hasil dari perintah select.
![Screenshot from 2023-06-25 19-47-55](https://github.com/mvinorian/hellscape/assets/54766683/27dc96ba-fb64-4460-a001-e5a48ada37b8)

# E. Logging

```c
int message_log(char *command)
{
    char log_file_path[BUFFER_SIZE];
    sprintf(log_file_path, "%s/%s.log", log_path, database_used);

    char *log_message = malloc(5000 * sizeof(char));
    time_t now = time(NULL);
    struct tm *t = localtime(&now);

    int year = t->tm_year;
    int month = t->tm_mon;
    int day = t->tm_mday;
    int hour = t->tm_hour;
    int minute = t->tm_min;
    int second = t->tm_sec;

    sprintf(log_message, "echo '%d-%02d-%02d %02d:%02d:%02d:%s:%s' >> '%s'\n", year + 1900, month + 1, day, hour, minute, second, user_client, command, log_file_path);
    system(log_message);

    return EXIT_SUCCESS;
}

```

Semua perintah yang dilakukan pada database maka akan tersimpan pada log khusus untuk database tersebut.

![Screenshot from 2023-06-26 06-09-43](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/d8c3e26f-fddb-478c-9a46-b03923c749a9)

hasil log:

![Screenshot from 2023-06-26 06-09-55](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/4f1540ab-a95b-4eb8-83ef-01f2a6bda89c)

# F. Reliability

kode databasedump:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <netinet/in.h>

#define BUFFER_SIZE 1024

#define ADDRESS "127.0.0.1"
#define PORT 8080

int main(int argc, char const *argv[])
{
    int sock = 0, valread;
    struct sockaddr_in address;
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if (inet_pton(AF_INET, ADDRESS, &serv_addr.sin_addr) <= 0)
    {
        printf("\nInvalid address or address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        printf("\nConnection Failed \n");
        return -1;
    }

    char user_name[BUFFER_SIZE];
    char user_password[BUFFER_SIZE];
    char database_name[BUFFER_SIZE];

    if (geteuid() == 0)
        strcpy(user_name, "root");
    else if (argc != 6 || (argc == 6 &&
                           strcmp(argv[1], "-u") != 0 &&
                           strcmp(argv[3], "-p") != 0))
    {
        printf(
            "%s\n%s %s\n",
            "\e[91mError\e[39m: Invalid command",
            argv[0],
            "-u \e[3muser_name\e[0m -p \e[3muser_password\e[0m \e[3mdatabase\e[0m");
        return EXIT_FAILURE;
    }
    else
    {
        strcpy(user_name, argv[2]);
        strcpy(user_password, argv[4]);
        strcpy(database_name, argv[5]);

        char request[BUFFER_SIZE], response[BUFFER_SIZE];
        sprintf(request, "%s;\nBACKUP %s AUTH USER %s IDENTIFIED BY %s;", user_name, database_name, user_name, user_password);
        send(sock, request, BUFFER_SIZE, 0);

        recv(sock, response, BUFFER_SIZE, 0);
        if (!strstr(response, "OK"))
        {
            printf("%s\n", response);
            return EXIT_FAILURE;
        }
    }

    return 0;
}


```

Database dump digunakan untuk melakukan backup command log ke dalam file backup.

![Screenshot from 2023-06-26 06-14-03](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/82e929b9-e414-4d4c-9199-183fc74cb649)

# G. Tambahan

Kita dapat mengunakan command pada backup untuk melakukan dml ataupun ddl. Contoh sebagai berikut:

![Screenshot from 2023-06-26 06-18-06](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/03ae25d1-2913-442f-b995-de3727e8a930)

hasil:
![Screenshot from 2023-06-26 06-18-35](https://github.com/dimss113/Modul1_Probstat_5025211010/assets/89715780/44fb2cf5-0a35-4f30-b08f-9b279f24de83)

# H. Error Handling

Berikut adalah beberapa contoh dari error handling yang telah dibuat.
![Screenshot from 2023-06-25 20-02-52](https://github.com/mvinorian/hellscape/assets/54766683/002f64ef-225b-4f72-8d0f-355029d42364)

# I. Contenairization

## I1. Dockerfile

Untuk menjalankan program server ke dalam container, akan dibuat image dengan base `ubuntu:focal`. Kemudian di dalam image tersebut akan menginstall gcc dan juga melakukan proses build kode program `database.c` menjadi program `database`. Setelah itu, port yang akan menjadi tempat berjalannya program server akan dibuka oleh container. Terakhir, perlu ditambahkan perintah yang akan dijalankan ketika container dimulai. Berikut adalah isi dari dockerfile yang telah dibuat.

```Docerfile
FROM ubuntu:focal

RUN apt-get update && apt-get upgrade -y

RUN apt-get install gcc -y

WORKDIR /app

COPY ./database/ /app/

RUN gcc database.c -o database.app

EXPOSE 8080

CMD [ "./database.app" ]

```

## I2. Publish image ke dockerhub

Dockerfile yang telah dibuat akan dilakukan proses build dengan perintah `docker build -t storage-app .`. Setelah berhasil di-build, docker image akan di-tag sebelum di-push ke dockerhub dengan perintah `docker tag storage-app mvinorian/storage-app`. Setelah itu, docker image akan di-push ke dockerhub dengan perintah `docker push storage-app`

Docker image dapat diakses [di sini](https://hub.docker.com/r/mvinorian/storage-app/).

## I3. Docker compose

Docker compose akan dibuat untuk menjalankan docker image yang telah dibuat menjadi 5 instances. Berikut adalah kode dari `docker-compose.yml`

```yml
version: "3"
services:
  storage-app:
    image: mvinorian/storage-app
    ports:
      - 8080-8084:8080
    deploy:
      mode: replicated
      replicas: 5
      restart_policy:
        condition: any
```

Perlu diperhatikan bahwa port yang akan digunakan sebanyak 5 port (8080-8084).

Berikut adalah hasil dari `docker compose up -d`.
![Screenshot from 2023-06-25 20-15-42](https://github.com/mvinorian/hellscape/assets/54766683/1235f621-0e63-42b9-94d7-e76e69d3c488)

### Kendala
---
1. Tidak ada kendala apapun.

### Revisi
---
> Program yang ditambahkan setelah soal shift dilakukan

1. Tidak ada, pada saat demo sudah lancar dan tidak ada tambahan dari aslab penguji
